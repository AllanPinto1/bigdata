import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Context {
	private ArrayList<String> input;
	private Map<String,Integer> midOutput;
	private ArrayList<String> finalOutput;
	/**
	 * J'ai fait le choix de prendre des array list directement et d'initier les valeurs
	 * S'il faut faire avec un fichier CSV, il suffirait de lire le fichier CSV avant l'init de l'arrayList INPUT
	 */
 	public Context() {
		this.setInput(new ArrayList<String>());
		this.setMidOutput(new HashMap<String,Integer>());
		this.setFinalOutput(new ArrayList<String>());
		this.initInput();
	}
	private void initInput(){
		this.input.add("A B C D E");
		this.input.add("X Y Z 2 1");
		this.input.add("F G H I J");
	}
	public ArrayList<String> getInput() {
		return input;
	}

	public void setInput(ArrayList<String> input) {
		this.input = input;
	}

	public Map<String,Integer> getMidOutput() {
		return midOutput;
	}

	public void setMidOutput(Map<String,Integer> output) {
		this.midOutput = output;
	}
	public ArrayList<String> getFinalOutput() {
		return finalOutput;
	}
	public void setFinalOutput(ArrayList<String> finalOutput) {
		this.finalOutput = finalOutput;
	}

}
