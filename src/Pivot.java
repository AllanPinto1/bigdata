import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.io.IOException;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat; 

/**
 * Map - Reduce
 * BigData
 * @author allan
 *
 */
public class Pivot {

	public class PivotMapper extends Mapper<LongWritable, Text, IntWritable, Text> {
		/**
		 * On map mot par mot chaque ligne en indiquant l'index de colonne o� ils se situent
		 * @param key
		 * 	Index de la ligne trait�
		 * @param value
		 * 	phrase � mapper
		 */
		@Override
		protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, IntWritable, Text>.Context context) throws IOException, InterruptedException {
			StringTokenizer line = new StringTokenizer(value.toString());
			int indexColonne=0;
			String longKey = Long.toString(key.get());
			String word;
			while(line.hasMoreTokens()){
				word= line.nextToken();
				context.write(new IntWritable(indexColonne), new Text(longKey+","+word));
				indexColonne++;
			}
		}
	}

	public class PivotReducer extends Reducer<IntWritable, Text, IntWritable, Text> {
		/**
		 * On r�cup�re les donn�es sort & shuffle 
		 * On reduce et cr�ons notre output final
		 * @param ctx
		 */
		@Override
		protected void reduce(IntWritable key, Iterable<Text> values, Context context) throws IOException, InterruptedException {

			Map<Long, String> map = new TreeMap<Long, String>();
			String line = "";
			//On r�cup�re les donn�es 
			for(Text value : values) {
				map.put(Long.parseLong(value.toString().split(",")[0]), value.toString().split(",")[1]);
			}

			for(Map.Entry<Long, String> entry : map.entrySet()) {
				line += entry.getValue() + ",";
			}
			line = line.substring(0, line.length()-1);

			context.write(key, new Text(line));
		}
	}
	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws Exception {
		if (args.length != 2) { 
			System.exit(-1); 
		}

		Job pivJob = new Job();
		pivJob.setJobName("TP Pivot CSV HADOOP"); 
		pivJob.setJarByClass(Pivot.class); 
		
		FileInputFormat.setInputPaths(pivJob, new Path(args[0])); 
		FileOutputFormat.setOutputPath(pivJob, new Path(args[1]));

		pivJob.setMapperClass(PivotMapper.class);
		pivJob.setReducerClass(PivotReducer.class);

		pivJob.setMapOutputKeyClass(IntWritable.class); 
		pivJob.setMapOutputValueClass(Text.class);

		pivJob.setOutputKeyClass(IntWritable.class); 
		pivJob.setOutputValueClass(Text.class);
		
		System.exit(pivJob.waitForCompletion(true) ? 0 : 1);
	}

}
