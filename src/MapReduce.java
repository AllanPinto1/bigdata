import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
/**
 * Map - Reduce
 * BigData
 * @author allan
 *
 */
public class MapReduce {

	public MapReduce() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * On map mot par mot chaque ligne en indiquant l'index de colonne o� ils se situent
	 * @param key
	 * 	Index de la ligne trait�
	 * @param value
	 * 	phrase � mapper
	 * @param ctx
	 * 	context o� on r�cup�re les listes 
	 */
	public void map(Integer key,String value,Context ctx){
		StringTokenizer line = new StringTokenizer(value);
		String word;
		Integer indexColonne=0;
		while(line.hasMoreTokens()){
			word= line.nextToken();
			ctx.getMidOutput().put(word, indexColonne);
			indexColonne++;
		}
	}
	/**
	 * On r�cup�re les donn�es sort & shuffle 
	 * On reduce et cr�ons notre output final
	 * @param ctx
	 */
	public void reduce(Context ctx){
		//On r�cup�re les donn�es shuffled & sorted
		HashMap<Integer, List<String>> sortedMap = this.draftShuffleSort(ctx);
		ArrayList<String> finalOutput = ctx.getFinalOutput();
		for (Integer key : sortedMap.keySet()) {
			String line ="";
			for(String word:sortedMap.get(key)){
				line+=word+" ";
			}
			finalOutput.add(line);
		}
	}
	
	public static void main(String[] args) {
		Context ctx = new Context();
		MapReduce mr= new MapReduce();
		Integer indexLigne=0;
		for (String line : ctx.getInput()) {
			mr.map(indexLigne, line, ctx);
			indexLigne++;
		}
		mr.reduce(ctx);
		System.out.println("Input \n" +mr.customToString(ctx.getInput()));
		System.out.println("Output \n" +mr.customToString(ctx.getFinalOutput()));

	}
	/**
	 * Afin de v�rifier que mon reduce fonctionne, je cr�� � un shuffle/sort 
	 * Premier temps : je r�cup�re les valeurs uniques de mon HashMap
	 * Second temps : on cr�� l'association <Cl�Uniques, List<String>>
	 * On utilise ensuite le HashMap<Cl�,List<String>> pour le reduce
	 * @param ctx : On y retrouve nos List d'INPUT et OUTPUT
	 * Return 
	 * 	List<Cl�, <List<String>>
	 * 		- Cl� : l'ancien emplacement sur la colonne
	 * 		- List<String>: les valeurs pr�sentes � cette colonne
	 */
	private HashMap<Integer, List<String>> draftShuffleSort(Context ctx){
		//On r�cup�re les valeurs uniques pour pouvoir faire le shuffle & sort 
		//Normalement fait par le framework
		Map<String, Integer> output = ctx.getMidOutput();
		HashSet<Integer> values =new HashSet<>();
		Iterator<Integer> itValues = ctx.getMidOutput().values().iterator();
		//On r�cup�re un Set de valeurs uniques
		while(itValues.hasNext()){
			values.add(itValues.next());
		}
		HashMap<Integer, List<String>> sortedMap= new HashMap<>();
		//On r�cup�re l'ensemble des cl�s pour cr�er notre tableau � partir des valeurs attribu�s � ces cl�s
		for (String key : ctx.getMidOutput().keySet()) {
			if(!sortedMap.containsKey(output.get(key))){
				ArrayList<String> listValues= new ArrayList<String>();
				listValues.add(key);
				sortedMap.put(output.get(key), listValues );
			}else{
				sortedMap.get(output.get(key)).add(key);
			}
		}
		return sortedMap;
	}
	
	private String customToString(List<?> list){
		String display ="";
		for (int i = 0; i < list.size(); i++) {
			display+=list.get(i)+"\n";
		}
		return display;
		
	}
}
